#ifndef AUTOPEN_H
#define AUTOPEN_H

#include "Pen.h"

class Autopen: public Pen {
private:
    bool open;
    
public:
    Autopen(int inkAmount);
    ~Autopen();
    
    void write(const std::string& message, Paper& paper);
    
    void click();
    
    bool isOpen() const;
};

std::ostream& operator<<(std::ostream& out, const Autopen &pen);

#endif
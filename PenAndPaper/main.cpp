#include <iostream>
#include <string>
#include "Autopen.h"
#include "Pen.h"
#include "Paper.h"

Pen_V_T {
    ~Pen("Pen")
    ~write("Pen")
}

Autopen_V_T {
    ~Autopen() {"Autopen"}
    ~write() {"Autopen"}
}

void write(const std::string& message, Pen *pen, Paper *paper) {
    pen->write(message, *paper);
}

int main() {
    Paper *paper = new Paper(300);
    Pen *pen = new Pen(500);
    Autopen *ap = new Autopen(500);
    Pen *pencilBox[] = { pen, ap };
    int count = sizeof(pencilBox) / sizeof(Pen *);
    
    std::cout << sizeof(*pen) << std::endl;
    std::cout << sizeof(*ap) << std::endl;
    
    ap->click();
    for (int i = 0; i < count; i++) {
        write("Hello, world!\n", pencilBox[i], paper);
    }
    
    paper->show();
    
    for (int i = 0; i < count; i++) {
        std::cout << *pencilBox[i] << std::endl;
    }
    
    for (int i = 0; i < count; i++) {
        delete pencilBox[i];
    }
    delete paper;
    
    return 0;
}

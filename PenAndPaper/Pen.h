#ifndef PEN_H
#define PEN_H

#include <string>
#include "Paper.h"
#include <iostream>

class Pen {
private:
    int inkAmount;
    int maxInkAmount;
    
public:
    Pen(int inkAmount);
    virtual ~Pen();
    
    virtual void write(const std::string& message, Paper& paper);
    
    int getInkAmount() const;
    int getMaxInkAmount() const;
};

std::ostream& operator<<(std::ostream& out, const Pen &pen);

#endif
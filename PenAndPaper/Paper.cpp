#include <iostream>
#include "Paper.h"
#include "Exceptions.h"

//MARK: - init

Paper::Paper(int capacity) {
    if (capacity <= 0) {
        throw InvalidParam();
    }
    this->capacity = capacity;
}

//MARK: - public
    
void Paper::addContent(const std::string& message) {
    if (message.length() > getFreeSpace()) {
        throw PaperOverflow();
    }
    content += message;
}

void Paper::show() const {
    std::cout << content << std::endl;
}

//MARK: getters

const std::string& Paper::getContent() const {
    return content;
}

int Paper::getCapacity() const {
    return capacity;
}

int Paper::getFreeSpace() const {
    return getCapacity() - content.length();
}

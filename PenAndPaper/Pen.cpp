#include "Pen.h"
#include "Exceptions.h"
#include <iostream>

Pen::Pen(int inkAmount) {
    if (inkAmount < 0) {
        throw InvalidParam();
    }
    this->inkAmount = inkAmount;
    this->maxInkAmount = inkAmount;
}

Pen::~Pen() {
    std::cout << "Pen::deinit" << std::endl;
}

void Pen::write(const std::string& message, Paper& paper) {
    std::cout << "Pen::write()" << std::endl;
    int newInkAmount = getInkAmount() - message.length();

    if (newInkAmount < 0) {
        throw OutOfInk();
    }
    std::cout << "* Scr-r-r-r-r *" << std::endl;
    inkAmount = newInkAmount;
    paper.addContent(message);
}

int Pen::getInkAmount() const {
    return inkAmount;
}

int Pen::getMaxInkAmount() const {
    return maxInkAmount;
}

std::ostream& operator<<(std::ostream& out, const Pen &pen) {
    return out << "Inks (" << pen.getInkAmount() << "/" << pen.getMaxInkAmount() << ")";
}

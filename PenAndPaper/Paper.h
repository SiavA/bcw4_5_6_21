#ifndef PAPER_H
#define PAPER_H

#include <string>

class Paper {
private:
    std::string content;
    int capacity;
    
public:
    Paper(int capacity);
    
    void addContent(const std::string& message);
    
    void show() const;
    
    const std::string& getContent() const;
    int getCapacity() const;
    int getFreeSpace() const;
};

#endif
#include "Autopen.h"
#include "Exceptions.h"
#include <iostream>

Autopen::Autopen(int inkAmount) : Pen(inkAmount) {
    this->open = false;
}

Autopen::~Autopen() {
    std::cout << "Autopen::deinit" << std::endl;
}

void Autopen::write(const std::string& message, Paper& paper) {
    std::cout << "Autopen::write()" << std::endl;
    if (!isOpen()) {
        throw Closed();
    }
    Pen::write(message, paper);
}

void Autopen::click() {
    std::cout << "* CLICK *" << std::endl;
    open = !isOpen();
}

bool Autopen::isOpen() const {
    return open;
}

std::ostream& operator<<(std::ostream& out, const Autopen &ap) {
    return out << (const Pen&) ap << " " << (ap.isOpen() ? "opened" : "closed");
}

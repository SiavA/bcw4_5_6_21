#ifndef TAMAGOCHI_H
#define TAMAGOCHI_H

#include <string>
#include "Feedable.h"

class Tamagochi: public Feedable {
private:
    std::string type;
    
public:
    Tamagochi(const std::string& type);
    virtual ~Tamagochi();
    
    virtual void eat();
    virtual void play();
};

#endif
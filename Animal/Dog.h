#ifndef DOG_H
#define DOG_H

#include "Animal.h"

class Dog: public Animal {
protected:
    virtual std::string getFeedSound() const;
    virtual int getEnergyCost() const;
    
public:
    Dog(const std::string& name, int age);
    virtual ~Dog();
    
    virtual void walk();
    virtual void eat();
    
    virtual void bite() const;
};

#endif

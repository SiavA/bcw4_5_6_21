#ifndef ANIMAL_H
#define ANIMAL_H

#include "Walkable.h"
#include "Feedable.h"

#include <string>

class Animal: public Walkable, public Feedable {
private:
    std::string name;
    int age;
    
protected:
    int hungryLevel;
    
    virtual std::string getFeedSound() const = 0;
    virtual int getEnergyCost() const = 0;
    
public:
    Animal(const std::string& name, int age);
    virtual ~Animal();
    
    virtual void eat() = 0;
    
    virtual const std::string& getName() const;
    virtual int getAge() const;
    virtual int getHungryLevel() const;
};

std::ostream& operator<<(std::ostream& out, const Animal& animal);

#endif
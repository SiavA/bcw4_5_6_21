#include "Dog.h"
#include <iostream>

Dog::Dog(const std::string& name, int age) : Animal(name, age) {}

Dog::~Dog() {}

void Dog::walk() {
    std::cout << getName() << " walking with it's master." << std::endl;
    hungryLevel += getEnergyCost();
}

void Dog::eat() {
    Animal::eat();
}

std::string Dog::getFeedSound() const {
    return "* Bark Bark *";
}

int Dog::getEnergyCost() const {
    return 20;
}

void Dog::bite() const {
    std::cout << "* Çlacen-Çlacen *" << std::endl;
}

#include <iostream>
#include "Animal.h"
#include "Cat.h"
#include "Dog.h"
#include "Tamagochi.h"

void feed(Feedable *an) {
    an->eat();
}

void feed(Feedable *eaters[], int size) {
    for (int i = 0; i < size; i++) {
        feed(eaters[i]);
        // feedable[i]->eat();
    }
}

int main() {
    Cat kuzia("Kuzia 🐈", 3);
    Dog jacky("Jacky 🐕", 12);
    Tamagochi chick("Chicken 🐣");
    Feedable *eaters[] = {&kuzia, &jacky, &chick};
    
    chick.play();
    
    std::cout << kuzia << std::endl;
    std::cout << jacky << std::endl;
    
    for (int i = 0; i < 2; i++) {
        kuzia.walk();
        jacky.walk();
    }
    
    std::cout << kuzia << std::endl;
    std::cout << jacky << std::endl;
    
    feed(eaters, 3);
    
    std::cout << kuzia << std::endl;
    std::cout << jacky << std::endl;
    
    return 0;
}

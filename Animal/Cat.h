#ifndef CAT_H
#define CAT_H

#include "Animal.h"

class Cat: public Animal {
protected:
    virtual std::string getFeedSound() const;
    virtual int getEnergyCost() const;
    
public:
    Cat(const std::string& name, int age);
    virtual ~Cat();
    
    virtual void walk();
    virtual void eat();
    
    virtual void scratch() const;
};

#endif

#include "Animal.h"
#include <iostream>

Animal::Animal(const std::string& name, int age) {
    this->name = name;
    this->age = age;
    this->hungryLevel = 0;
}

Animal::~Animal() {}

void Animal::eat() {
    int newHungryLevel = getHungryLevel() - getEnergyCost();
    
    std::cout << getFeedSound() << std::endl;
    
    if (newHungryLevel < 0) {
        newHungryLevel = 0;
    }
    hungryLevel = newHungryLevel;
}

const std::string& Animal::getName() const {
    return name;
}

int Animal::getAge() const {
    return age;
}

int Animal::getHungryLevel() const {
    return hungryLevel;
}

std::ostream& operator<<(std::ostream& out, const Animal& animal) {
    return out  << "'" << animal.getName()
                << "' age: " << animal.getAge()
                << " hungry level: " << animal.getHungryLevel();
}

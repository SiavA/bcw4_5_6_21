#ifndef WALKABLE_H
#define WALKABLE_H

class Walkable {
public:
    virtual void walk() = 0;
};

#endif

#ifndef FEEDABLE_H
#define FEEDABLE_H

class Feedable {
public:
    virtual void eat() = 0;
};

#endif

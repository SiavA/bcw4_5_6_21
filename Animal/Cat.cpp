#include "Cat.h"
#include <iostream>

Cat::Cat(const std::string& name, int age) : Animal(name, age) {}

Cat::~Cat() {}

void Cat::walk() {
    std::cout << getName() << " walking alone." << std::endl;
    hungryLevel += getEnergyCost();
}

void Cat::eat() {
    Animal::eat();
}

std::string Cat::getFeedSound() const {
    return "Pur-r-r-r-r";
}

int Cat::getEnergyCost() const {
    return 10;
}

void Cat::scratch() const {
    std::cout << "* Scratch *" << std::endl;
}

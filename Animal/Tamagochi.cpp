#include "Tamagochi.h"
#include <iostream>

Tamagochi::Tamagochi(const std::string& type) {
    this->type = type;
}

Tamagochi::~Tamagochi() {}

void Tamagochi::eat() {
    std::cout << type << ": * Pi - Pi *" << std::endl;
}

void Tamagochi::play() {
    std::cout << type << ": * Pi po - pu Pi *" << std::endl;
}

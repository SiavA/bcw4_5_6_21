public class OutOfInkException extends PenAndPaperException { 
    public OutOfInkException() {
        super("Out of inks.");
    }
}
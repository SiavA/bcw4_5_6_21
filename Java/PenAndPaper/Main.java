import java.io.IOException;

final public class Main {
    final public static void main(String... args) {
        try {
            Paper paper = new Paper(1200);
            Pen pen = new Pen(300);
            Autopen ap = new Autopen(200);
            Pencil pencil = new Pencil(120);
            
            ap.click();
            
            pen.write("Hello, Pen!\n", paper);
            ap.write("Hello, Autopen!\n", paper);
            pencil.write("Hello, Pencil!\n", paper);
            
            paper.show();
            
            System.out.println(pen);
            System.out.println(ap);
            System.out.println(pencil);
            
            pen.refill();
            ap.refill();
            
            System.out.println(pen);
            System.out.println(ap);
            System.out.println(pencil);
        } catch (PenAndPaperException ex) {
            ex.printStackTrace();
        }
    } 
}

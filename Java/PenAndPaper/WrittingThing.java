public abstract class WrittingThing implements Writtable {
    private int absInkAmount;
    
    public WrittingThing(int inkAmount) {
        absInkAmount = inkAmount;
    }
    
    public abstract String getName();
    
    public void write(String message, Paper paper) throws PenAndPaperException {
        int newInkAmount = this.getInkAmount() - message.length();
        
        if (newInkAmount < 0) {
            throw new OutOfInkException();
        }
        paper.addContent(message);
        this.setInkAmount(newInkAmount);
    }
    
    public int getInkAmount() {
        return this.absInkAmount;
    }
    
    public String toString() {
        return this.getName() + " ink: " + getInkAmount();
    }
    
    protected void setInkAmount(int inkAmount) {
        this.absInkAmount = inkAmount;
    }
}

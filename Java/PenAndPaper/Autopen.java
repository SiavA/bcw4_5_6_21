public class Autopen extends Pen {
    private boolean open;
    
    public Autopen(int inkAmount) {
        super(inkAmount);
        this.open = false;
    }
    
    public void write(String message, Paper paper) throws PenAndPaperException {
        if (!this.isOpen()) {
            throw new ClosedPenException();
        }
        super.write(message, paper);
    }
    
    public void click() {
        System.out.println("* CLICK *");
        this.open = !isOpen();
    }
    
    public String toString() {
        return super.toString() + " " + (this.isOpen() ? "opened" : "closed");
    }
    
    public String getName() {
        return "Autopen";
    }
    
    public boolean isOpen() {
        return this.open;
    }
}
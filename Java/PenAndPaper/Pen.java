public class Pen extends WrittingThing {
    private int maxInkAmount;
    
    public Pen(int inkAmount) {
        super(inkAmount);
        this.maxInkAmount = inkAmount;
    }
    
    public void refill() {
        this.setInkAmount(maxInkAmount);
    }
    
    public String toString() {
        return super.toString() + " of " + getMaxInkAmount();
    }
    
    public String getName() {
        return "Pen";
    }
    
    public int getMaxInkAmount() {
        return this.maxInkAmount;
    }
}
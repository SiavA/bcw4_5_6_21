public abstract class PenAndPaperException extends Exception {
    public PenAndPaperException(String message) {
        super(message);
    }
}

public interface Writtable {
    public void write(String message, Paper paper) throws PenAndPaperException;
}
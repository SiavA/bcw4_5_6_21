public class ClosedPenException extends PenAndPaperException { 
    public ClosedPenException() {
        super("Pen can't write closed.");
    }
}

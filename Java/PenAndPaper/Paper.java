import java.io.IOException;

public class Paper {
    private StringBuilder content = new StringBuilder("");
    private int capacity;
    
    public Paper(int capacity) {
        this.capacity = capacity;
    }
    
    public void show() {
        System.out.println(content);
    }
    
    public void addContent(String message) throws PenAndPaperException {
        if (message.length() > getFreeSpace()) {
            throw new OutOfSpaceException(getFreeSpace(), message.length());
        }
        this.content.append(message);
    }
    
    public int getCapacity() {
        return capacity;
    }
    
    public int getFreeSpace() {
        return capacity - content.length();
    }
    
    public String getContent() {
        return content.toString();
    }
}

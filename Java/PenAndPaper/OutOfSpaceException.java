public class OutOfSpaceException extends PenAndPaperException { 
    public OutOfSpaceException(int freeSpace, int messageLength) {
        super("Out of space exception. It's only " + freeSpace + " free space, but message has " + messageLength + " length.");
    }
}
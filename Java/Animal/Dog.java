public class Dog extends Animal {
    public Dog(String name, int age) {
        super(name, age);
    }
    
    public void walk() {
        hungryLevel += getEnergyCost();
        System.out.println(getName() + " walking with it's master.");
    }
    
    public String getFeedSound() {
        return "* Bark Bark *";
    }
    
    public int getEnergyCost() {
        return 20;
    }
}

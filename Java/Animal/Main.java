final public class Main {
    //🐣 🐈 🐕
    private static void feed(Feedable eater) {
        eater.eat();
    }
    
    private static void feed(Feedable[] eaters) {
        for (Feedable eater : eaters) {
            feed(eater);
        }
    }
    
    final public static void main(String... arg) {
        Cat kuzia = new Cat("Kuzia 🐈", 13);
        Dog jacky = new Dog("Jacky 🐕", 2);
        Tamagochi chick = new Tamagochi("Chicken 🐣");
        Feedable[] eaters = { kuzia, jacky, chick };
        
        kuzia.walk();
        jacky.walk();
        chick.play();
        
        System.out.println(kuzia);
        System.out.println(jacky);
        
        feed(eaters);
        
        System.out.println(kuzia);
        System.out.println(jacky);
        System.out.println(chick);
    }
}

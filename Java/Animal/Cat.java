public class Cat extends Animal {
    public Cat(String name, int age) {
        super(name, age);
    }
    
    public void walk() {
        hungryLevel += getEnergyCost();
        System.out.println(getName() + " walking alone.");
    }
    
    public String getFeedSound() {
        return "* Pur-r-r-r *";
    }
    
    public int getEnergyCost() {
        return 10;
    }
}

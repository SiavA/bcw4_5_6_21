public class Tamagochi implements Feedable {
    private String type;
    
    public Tamagochi(String type) {
        this.type = type;
    }
    
    public void eat() {
        System.out.println("* Pi-pI *");
    }
    
    public void play() {
        System.out.println("* Pi-pu po-Pi *");
    }
    
    public String getType() {
        return type;
    }
    
    public String toString() {
        return this.getType();
    }
}
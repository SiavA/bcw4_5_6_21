public abstract class Animal implements Walkable, Feedable {
    private String name;
    private int age;
    
    protected int hungryLevel;
    
    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
        this.hungryLevel = 0;
    }
    
    public abstract String getFeedSound();
    
    public abstract int getEnergyCost();
    
    public void eat() {
        int newHungryLevel = this.getHungryLevel() - this.getEnergyCost();
        
        System.out.println(this.getFeedSound());
        
        if (newHungryLevel < 0) {
            newHungryLevel = 0;
        }
        this.hungryLevel = newHungryLevel;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int getAge() {
        return this.age;
    }
    
    public int getHungryLevel() {
        return this.hungryLevel;
    }
    
    public String toString() {
        StringBuilder result = new StringBuilder(this.getName());
        
        result.append(" age: " + this.getAge());
        result.append(" hungry level: " + this.getHungryLevel());
        
        return result.toString();
    }
}

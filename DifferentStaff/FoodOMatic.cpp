#include <iostream>
#include <vector>

class Food {
private:
    std::string title;
public:
    Food(const std::string& title) {
        this->title = title;
    }
    
    const std::string& getTitle() const {
        return title;
    }
};

class FoodProducer {
public:
    virtual Food *produceFood() = 0;
    virtual ~FoodProducer() {}
};

class FoodProgramm: public FoodProducer {
public:
    virtual ~FoodProgramm() {}
};

class EggsBoilProgramm: public FoodProgramm {
public:
    EggsBoilProgramm() {}
    virtual ~EggsBoilProgramm() {}
    
    virtual Food *produceFood() {
        std::cout << "Boiling eggs" << std::endl;
        return new Food("Boiled eggs");
    }
};

class EggsFryProgramm: public FoodProgramm {
public:
    EggsFryProgramm() {}
    virtual ~EggsFryProgramm() {}
    
    virtual Food *produceFood() {
        std::cout << "Frying eggs" << std::endl;
        return new Food("Fried eggs");
    }
};

class FishBakeProgramm: public FoodProgramm {
public:
    FishBakeProgramm() {}
    virtual ~FishBakeProgramm() {}
    
    virtual Food *produceFood() {
        std::cout << "Baking fish" << std::endl;
        return new Food("Baked fish");
    }
};

class FoodOMatic: public FoodProducer {
private:
    int currentIdx;
    std::vector<FoodProgramm *> programms;
    
public:
    FoodOMatic() {
        programms.push_back(new EggsBoilProgramm());
        programms.push_back(new EggsFryProgramm());
        programms.push_back(new FishBakeProgramm());
        currentIdx = 0;
    }
    
    virtual ~FoodOMatic() {
        for (int i = 0; i < programms.size(); i++) {
            delete programms[i];
        }
    }
    
    virtual Food *produceFood() {
        return programms[currentIdx]->produceFood();
    }
    
    virtual void goNext() {
        int nextIndex = currentIdx + 1;
        
        if (nextIndex >= programms.size()) {
            nextIndex %= programms.size();
        }
        currentIdx = nextIndex;
    }
};

void produceVia(FoodProducer& cooker) {
    Food *food = cooker.produceFood();
    
    std::cout << "Did produce " << food->getTitle() << std::endl;
    
    delete food;
}

int main() {
    FoodOMatic cooker;
    
    produceVia(cooker);
    cooker.goNext();
    produceVia(cooker);
    cooker.goNext();
    produceVia(cooker);
    cooker.goNext();
    produceVia(cooker);
    
    return 0;
}

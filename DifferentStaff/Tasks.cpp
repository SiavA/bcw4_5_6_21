#include <iostream>
#include <list>

class TitlePrototype {
    virtual std::string getTitle() = 0;
}

class Movie {
private:
    std::string title;
public:
    virtual std::string getTitle() {
        return title;
    }
}

class Song {
private:
    std::string title;
    
public:
    Song(TitlePrototype prot) {
        this->title = prot.getTitle();
    }
}


class CustomStringConvertible {
public:
    virtual std::string toString() const = 0;
};

class Processable: public CustomStringConvertible {
public:
    virtual ~Processable() {}
    
    virtual void process() = 0;
    virtual bool isDone() const = 0;
};

class Task: public Processable {
private:
    std::string title;
    bool done;
public:
    Task(const std::string& title) {
        this->title = title;
        this->done = false;
    }
    
    virtual ~Task() {}
    
    virtual const std::string& getTitle() const {
        return title;
    }
    
    virtual void process() {
        std::cout << "Processing " << getTitle() << std::endl;
        done = true;
    }
    
    virtual bool isDone() const {
        return done;
    }
    
    virtual std::string toString() const {
        return getTitle() + "[" + (isDone() ? "Done" : "TODO") + "]";
    }
};

class ComplexTask: public Task {
private:
    std::string statement;
public:
    ComplexTask(const std::string title, const std::string statement) : Task(title) {
        this->statement = statement;
    }
    
    virtual ~ComplexTask() {}
    
    virtual const std::string& getStatement() const {
        return statement;
    }
    
    virtual void process() {
        Task::process();
        std::cout << "\tWith statement: " << getStatement() << std::endl;
    }
    
    virtual std::string toString() const {
        return Task::toString() + "\n\tStatement: " + getStatement();
    }
};

class TaskList: public Processable {
private:
    std::list<Processable *> tasks;
    bool done;
public:
    TaskList(const std::list<Processable *>& tasks) {
        this->tasks = tasks;
        this->done = false;
    }
    
    virtual ~TaskList() {}
    
    virtual void process() {
        for (std::list<Processable *>::iterator it = tasks.begin(); it != tasks.end(); it++ ) {
            (*it)->process();
        }
        done = true;
    }
    
    virtual bool isDone() const {
        return done;
    }
    
    virtual std::string toString() const {
        std::string doneSuffix = (isDone() ? "Done" : "TODO");
        std::string base = "Tasks list [" + doneSuffix + "]:\n";
    
        for (std::list<Processable *>::const_iterator it = tasks.begin(); it != tasks.end(); it++ ) {
            base += "\t* " + (*it)->toString() + "\n";
        }
    
        return base;
    }
};

std::list<Processable *> createPolySubtasks() {
    std::list<Processable *> subTasks;
    
    subTasks.push_back(new Task("Describe compile-time poly"));
    subTasks.push_back(new ComplexTask("Describe runtime poly", "Describe inheritance"));
    subTasks.push_back(new ComplexTask("Describe parametric poly", "Describe Generics"));
    
    return subTasks;
}

std::list<Processable *> createAll(std::list<Processable *> polySubtasks) {
    std::list<Processable *> all;
    
    all.push_back(new Task("Implement Animal"));
    all.push_back(new Task("Implement PenAndPaper"));
    all.push_back(new ComplexTask("Show Polymorphism", "Show all 3 cases."));
    all.push_back(new TaskList(polySubtasks));
    
    return all;
}

void deleteTasks(std::list<Processable *> tasks) {
    for (std::list<Processable *>::iterator it = tasks.begin(); it != tasks.end(); it++ ) {
        delete *it;
    }
}

int main() {
    std::list<Processable *> polySubtasks = createPolySubtasks();
    std::list<Processable *> all = createAll(polySubtasks);
    
    {
        TaskList allTasks(all);
    
        std::cout << allTasks.toString() << std::endl;
        
        allTasks.process();
        
        std::cout << allTasks.toString() << std::endl;
    }
    
    deleteTasks(polySubtasks);
    deleteTasks(all);
    
    return 0;
}



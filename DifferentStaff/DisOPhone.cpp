#include <iostream>
#include <string>

class Playable {
public:
    virtual ~Playable() {}
    
    virtual void play() = 0;
};

class MusicDisc: public Playable {
private:
    std::string song;
    
public:
    MusicDisc(const std::string& song) {
        this->song = song;
    }
    
    virtual ~MusicDisc() {}
    
    virtual void play() {
        std::cout << "🎵 " << song << " 🎶" << std::endl;
    }
};

class DiskOPhone: public Playable {
private:
    MusicDisc *current;
public:
    DiskOPhone() {
        this->current = NULL;
    }
    
    virtual ~DiskOPhone() {}
    
    virtual void play() {
        if (current != NULL) {
            current->play();
        } else {
            std::cout << "Tc-sc" << std::endl;
        }
    }
    
    virtual void insertDisc(MusicDisc *disk) {
        this->current = disk;
    }
};

int main() {
    MusicDisc *shal = new MusicDisc("Shala-la-la la-la");
    MusicDisc *olo = new MusicDisc("Olo-lo Olo-lo");
    MusicDisc *loza = new MusicDisc("Under the roof");
    DiskOPhone player;
    
    player.play();
    
    player.insertDisc(loza);
    
    player.play();
    
    player.insertDisc(olo);
    
    player.play();
    
    delete shal;
    delete olo;
    delete loza;
    
    return 0;
}
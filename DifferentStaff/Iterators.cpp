#include <iostream>

template <typename T>
class Iterable {
public:
    virtual ~Iterable() {}
    virtual T getCurrent() const = 0;
    virtual bool next() = 0;
};

class RangeIterator: public Iterable<int> {
public:
    int start;
    int end;
    int current;
    
    RangeIterator(int start, int end) {
        this->start = start;
        this->end = end;
        this->current = start - 1;
    }
    
    virtual ~RangeIterator() {}
    
    virtual int getCurrent() const {
        return current;
    }
    
    virtual bool next() {
        int next = current + 1;

        if (next == end) {
            return false;
        }
        current = next;
        return true;
    }
};

class AriProgIterator: public Iterable<int> {
public:
    int start;
    int end;
    int step;
    int current;
    
    AriProgIterator(int start, int end, int step) {
        this->start = start;
        this->end = end;
        this->step = step;
        this->current = start - step;
    }
    
    virtual ~AriProgIterator() {}
    
    virtual int getCurrent() const {
        return current;
    }
    
    virtual bool next() {
        int next = current + step;

        if (next > end) {
            return false;
        }
        current = next;
        return true;
    }
};

class WordIterator: public Iterable<char> {
private:
    std::string word;
    int currentIndex;
public:
    WordIterator(const std::string& word) {
        this->word = word;
        this->currentIndex = -1; 
    }
    
    virtual ~WordIterator() {}
    
    virtual char getCurrent() const {
        return word[currentIndex];
    }
    
    virtual bool next() {
        int next = currentIndex + 1;
        
        if (next >= word.length()) {
            return false;
        }
        currentIndex = next;
        return true;
    }
};

template <typename T>
class ArrayIterator: public Iterable<T> {
private:
    int index;
    int count;
    T *arr;
    
public:
    ArrayIterator(T *arr, int count) {
        this->arr = arr;
        this->count = count;
        this->index = -1;
    }
    
    virtual ~ArrayIterator() {}
    
    virtual T getCurrent() const {
        return arr[index];
    }
    
    virtual bool next() {
        int next = index + 1;
        
        if (next >= count) {
            return false;
        }
        index = next;
        return true;
    }
};

template <typename T>
void foo(T arr[]) {
    
}

template <typename T>
void applyIterable(Iterable<T>& it) {
    while (it.next()) {
        std::cout << it.getCurrent() << std::endl;
    }
}

int main() {
    Iterable<int> *range = new RangeIterator(0, 20);
    Iterable<int> *ariProg = new AriProgIterator(0, 20, 1);
    Iterable<char> *hello = new WordIterator("Hello, world!");
    Iterable<char> *name = new WordIterator("My name is ...");
    double arr[] = {2.0, 18.9, 100.5, 36.6, -2.18};
    Iterable<double> *dbl1 = new ArrayIterator<double>(arr, 5);
    Iterable<double> *dbl2 = new ArrayIterator<double>(arr, 3);
    Iterable<char> *chr = new ArrayIterator<char>("Hello, world!", 13);
    

    applyIterable(*range);
    applyIterable(*ariProg);
    applyIterable(*hello);
    applyIterable(*name);
    applyIterable(*dbl1);
    applyIterable(*dbl2);
    applyIterable(*chr);
    
    delete range;
    delete ariProg;
    delete hello;
    delete name;
    delete dbl1;
    delete dbl2;
    delete chr;
    
    return 0;
}

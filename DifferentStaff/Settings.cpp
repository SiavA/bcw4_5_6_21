#include <iostream>

class Preferences {
private:
    static Preferences *shared;
    
    Preferences(double width, double height, std::string workDir, int tasksPerPage) {
        this->width = width;
        this->height = height;
        this->workDir = workDir;
        this->tasksPerPage = tasksPerPage;
    }
    
public:
    double width;
    double height;
    std::string workDir;
    int tasksPerPage; 
    
    static Preferences *getSharedPreferences() {
        if (shared == NULL) {
            shared = new Preferences(320.0, 240.0, "/var/Bender/work_dir", 12);
        }
        return shared;
    }
};

Preferences *Preferences::shared = NULL;

void showTasks() {
    Preferences *pref = Preferences::getSharedPreferences();
    
    std::cout << pref->tasksPerPage << " tasks on " << pref->width << "x" << pref->height << std::endl;
}

void config() {
    Preferences *pref = new Preferences(.0, 240.0, "/var/Bir", 12);
    double width, height;
    
    std::cout << "Enter new width and height" << std::endl;
    std::cin >> width;
    std::cin >> height;
    
    pref->width = width;
    pref->height = height;
}

int main() {
    showTasks();
    
    config();
    
    showTasks();
    
    return 0;
}
